package main

import (
	"flag"
	"reflect"
	"testing"

	"github.com/urfave/cli/v2"
)

func TestArgs(t *testing.T) {
	t.Run("when no flags are provided", func(t *testing.T) {
		set := flag.NewFlagSet("test", 0)
		c := cli.NewContext(nil, set, nil)

		got := retireArgs(c)
		want := []string{"--outputformat", "jsonsimple", "--outputpath", "retire.json", "--exitwith", "0"}
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the --insecure flag is provided", func(t *testing.T) {
		set := flag.NewFlagSet("test", 0)
		c := cli.NewContext(nil, set, nil)
		set.Bool("advisory-db-insecure", true, "")

		got := retireArgs(c)
		want := []string{"--outputformat", "jsonsimple", "--outputpath", "retire.json", "--exitwith", "0",
			"--insecure"}
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the --js-advisory-db and --node-advisory-db flags are provided", func(t *testing.T) {
		set := flag.NewFlagSet("test", 0)
		c := cli.NewContext(nil, set, nil)
		set.String("js-advisory-db", "js-advisory-db-path", "")
		set.String("node-advisory-db", "node-advisory-db-path", "")

		got := retireArgs(c)
		want := []string{"--outputformat", "jsonsimple", "--outputpath", "retire.json", "--exitwith", "0",
			"--jsrepo", "js-advisory-db-path", "--noderepo", "node-advisory-db-path"}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
